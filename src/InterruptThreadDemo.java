class InterruptThreadDemo {
	public static void main(String[] args) throws InterruptedException {
		MyThread m = new MyThread(); // 创建线程对象m
		System.out.println("Starting thread...");
		m.start();// 启动线程m
		Thread.sleep(3000);
		System.out.println("Interrupt thread...");
		m.stop = true; // 修改共享变量
		m.interrupt(); // 阻塞时退出阻塞状态
		Thread.sleep(3000);
		System.out.println("Stopping application...");
	}
}