class MyThread extends Thread {
	volatile boolean  stop = false; // 引入一个布尔型的共享变量stop

	public void run() {
		while (!stop) // 通过判断stop变量的值来确定是否继续执行线程体
		{
			System.out.println(getName() + " is running");
			try {
				sleep(1000);
			} catch (InterruptedException e) {
				System.out.println(e.getMessage());
			}
		}
		System.out.println("Thread is exiting...");
	}
}